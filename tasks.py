# we are importing Celery class from celery package
from celery import Celery
from scrapper import boletim_de_urna

# Redis broker URL
BROKER_URL = 'amqp://guest:guest@192.168.1.90:5672'
BACKEND_URL = 'redis://192.168.1.90:6379'

# We are creating an instance of Celery class by passing module name as Restaurant and broker as Redis.
celery_app = Celery('Restaurant', broker=BROKER_URL, backend=BACKEND_URL)

# we are decorating cooking_task function with @celery_app.task decorator.
# Functions which are decorated with @celery_app.task considered celery tasks.
@celery_app.task
def cooking_task(table_no, dishes, turno=None, uf=None, municipio=None, zona=None, secao=None):
    print("Started cooking for Table Number : "+table_no)
    for dish in dishes:
        print("Cooking : "+dish)
    print("Done cooking for Table Number : "+table_no)

@celery_app.task
def download_bus_task(turno, uf, municipio, zona, secao):
    boletim_de_urna(turno, uf, municipio, zona, secao)
    print(f"Finalizada urna em {uf}-{municipio} zona {zona} seção {secao}.")

