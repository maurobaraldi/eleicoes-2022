import sqlite3
from json import loads
from glob import glob

con = sqlite3.connect("./arquivos/eleicoes-2022.db")
cur = con.cursor()

# Mude o valor para True se quiser popular o banco de dados com os dados dos arquivos.
popular_db = True

print("Preparando tabela de zonas e seções")
# Cria tabela de zonas e seções
cur.execute('''
    CREATE TABLE if not exists "secoes" (
        "uf"	TEXT NOT NULL,
        "estado"	TEXT NOT NULL,
        "nome"	TEXT NOT NULL,
        "codigo"	TEXT NOT NULL,
        "zona"	TEXT NOT NULL,
        "secao"	TEXT NOT NULL
    );
''')

# Cria tabela de resultados por UF (totalizadores)
cur.execute('''
    CREATE TABLE if not exists "resultado_uf" (
        "ano"	INTEGER NOT NULL,
        "turno"	INTEGER NOT NULL,
        "uf"	INTEGER NOT NULL,
        "secoes"	INTEGER NOT NULL,
        "eleitores"	INTEGER NOT NULL,
        "comparecimento"	INTEGER NOT NULL,
        "abstencao"	INTEGER NOT NULL,
        "validos"	INTEGER NOT NULL,
        "anulados"	INTEGER NOT NULL,
        "anulados_judice"	INTEGER NOT NULL,
        "nulos"	INTEGER NOT NULL,
        "brancos"	INTEGER NOT NULL,
        "anulados_separados"	INTEGER NOT NULL
    );
''')

if popular_db is True:

    # Popula tabela de municipios, zonas e seções
    values = []
    for uf in glob("./arquivos/zonas-secoes/zonas-secoes-[a-z]*.json"):
        print(f"  Carregando zonas e seçoes da UF: {uf[-7:-5].upper()}")
        with open(uf) as fp:
            estados = loads(fp.read())["abr"]
            for _estado in estados:
                uf = _estado["cd"]
                estado = _estado["ds"]
                municipios = _estado["mu"]
                for municipio in municipios:
                    codigo = municipio["cd"]
                    nome = municipio["nm"]
                    zonas = municipio["zon"]
                    for _zona in zonas:
                        zona = _zona["cd"]
                        secoes = _zona["sec"]
                        for _secao in secoes:
                            secao = _secao["ns"]
                            values.append([uf, estado, nome, codigo, zona, secao])

        # Adiciona todos os registros de uma vez para melhorar a performance
        cur.executemany("INSERT INTO secoes VALUES(?, ?, ?, ?, ?, ?)", values)

    # Popula tabela de resultados por UF (totalizadores)
    values = []
    for turno in ["primeiro", "segundo"]:
        for uf in glob(f"./arquivos/{turno}-turno/resultado-[a-z]*.json"):
            print(f"  Carregando resultados por totalizadores UF: {uf[-7:-5].upper()} {turno} turno.")
            with open(uf) as fp:
                resultado = loads(fp.read())
                #import pdb; pdb.set_trace()
                ano = resultado["dg"][-4:]
                turno = resultado["t"]
                uf = resultado["cdabr"]
                secoes = resultado["s"]
                eleitores = resultado["e"]
                comparecimento = resultado["c"]
                abstencao = resultado["a"]
                validos = resultado["vv"]
                anulados = resultado["van"]
                anulados_judice = resultado["vansj"]
                nulos = resultado["vn"]
                brancos = resultado["vb"]
                anulados_separados = resultado["vp"]
                values.append([ano, turno, uf, secoes, eleitores, comparecimento, abstencao, validos, anulados, anulados_judice, nulos, brancos, anulados_separados])

    # Adiciona todos os registros de uma vez para melhorar a performance
    cur.executemany("INSERT INTO resultado_uf VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values)

con.commit()
