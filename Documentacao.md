# Eleições Presidenciais

Todos os aqurivos podem ser encontrados na pasta aruqivos

## Lista de Municipios por Estados

Formato: JSON
URL: https://resultados.tse.jus.br/oficial/ele2022/544/config/mun-e000544-cm.json
Arquivo: mun-e000544-cm.json

## Dados Simplificados

### Resultados por estado

Há um padrão na nomenclatura das URLs e nomes dos arquivos. Isso para o primeiro turno.

O prefixo da URL é https://resultados.tse.jus.br/oficial/ele2022/544/dados-simplificados/ seguido da abreviação do estado e ac/, al, etc e por último o nome do arquivo com `abreviação do estado-c0001-e000544.json`

Exemplos:

https://resultados.tse.jus.br/oficial/ele2022/544/dados-simplificados/ac/ac-c0001-e000544-r.json

https://resultados.tse.jus.br/oficial/ele2022/544/dados-simplificados/al/al-c0001-e000544-r.json

Para o segundo turno segue o padrão

https://resultados.tse.jus.br/oficial/ele2022/547/dados-simplificados/al/al-c0001-e000545-r.json

## Lista de Zonas e Seções eleitorais por estado

https://resultados.tse.jus.br/oficial/ele2022/arquivo-urna/406/config/ac/ac-p000406-cs.json

## Arquivos de Boletins de Urna - Arquivos BU

São 5763 nunicipios que têm zonas de votação (incluindo as estrangeiras), 6283 zonas e 472075 seções por todo o Brazil. Aproximadamente 4 GB de dados.

### Codificação da URL arquivo auxiliar.

#### URL de exemplo

https://resultados.tse.jus.br/oficial/ele2022/arquivo-urna/406/dados/ce/13013/0026/0003/p000406-ce-m13013-z0026-s0003-aux.json

ce = unidade federativa (Ceara)
13013 = codigo do municipio (Abaiara)
0026 = número da zona 
0003 = número da seção
p000406 = constante


#### Nomenclatura do arquivo auxiliar.

Os arquivos auxiliares têm o hash que completa o endereço (URL) do arquivo de BU.

p000406 = constante
ce = unidade federativa (Ceara)
m13013 = constante m (municipio) + codigo do municipio (Abaiara)
z0026 = constante z (zona) + número da zona
s0003 = constante s (seção) + número da seção
-aux = constante
.json = extensão

#### Estrutura do arquivo auxiliar.

### Codificação da URL arquivo de BU.

https://resultados.tse.jus.br/oficial/ele2022/arquivo-urna/406/dados/ce/13013/0026/0004/696733487550794a596e6333682d54545a4d694445414349776d4a515165386c31652d6c2b514a6338794d3d/o00406-1301300260004.bu

## Arquivos de Registro de Votos - Arquivos RDV

#### URL de exemplo

https://resultados.tse.jus.br/oficial/ele2022/arquivo-urna/406/dados/rr/03085/0008/0001/316d36586457494532316d77345975335a70704f574c464659724d4c744446444969506d33376c385662413d/o00406-0308500080001.rdv

## Contabilização

### Estados e Municipios

Lista de municipios por estado segundo o TSE

TOCANTINS com 139 municipios
ALAGOAS com 102 municipios
PARAÍBA com 223 municipios
RIO GRANDE DO NORTE com 167 municipios
RONDÔNIA com 52 municipios
AMAZONAS com 62 municipios
SERGIPE com 75 municipios
DISTRITO FEDERAL com 1 municipios
SÃO PAULO com 645 municipios
SANTA CATARINA com 295 municipios
ESPÍRITO SANTO com 78 municipios
PIAUÍ com 224 municipios
BAHIA com 417 municipios
PERNAMBUCO com 185 municipios
RIO DE JANEIRO com 92 municipios
MINAS GERAIS com 853 municipios
RIO GRANDE DO SUL com 497 municipios
MATO GROSSO DO SUL com 79 municipios
RORAIMA com 15 municipios
AMAPÁ com 16 municipios
PARANÁ com 399 municipios
CEARÁ com 184 municipios
MARANHÃO com 217 municipios
GOIÁS com 246 municipios
MATO GROSSO com 141 municipios
PARÁ com 144 municipios
EXTERIOR com 181 municipios
ACRE com 22 municipios

### Zonas e seções.

A partir dos arquivos de zonas e seções encontrados na pasta arquivos foram encontadas as totalizações de estados X municipios x zonas x seções.

Obs. ZZ é a sigla para localidades estrangeiras.


| estado | municipios | zonas | seçoes |
|--------|------------|-------|--------|
|   AC   |     22     |  23   |  2124  |
|   AL   |     102    |  107  |  6626  |
|   AM   |     62     |  74   |  7454  |
|   AP   |     16     |  17   |  1740  |
|   BA   |     417    |  450  |  34424 |
|   CE   |     184    |  205  |  22796 |
|   DF   |     1      |  19   |  6748  |
|   ES   |     78     |  85   |  9239  |
|   GO   |     246    |  259  |  14620 |
|   MA   |     217    |  223  |  16423 |
|   MG   |     853    |  898  |  49981 |
|   MS   |     79     |  88   |  6912  |
|   MT   |     141    |  146  |  7652  |
|   PA   |     144    |  159  |  18235 |
|   PB   |     223    |  230  |  9602  |
|   PE   |     185    |  209  |  20572 |
|   PI   |     224    |  230  |  8963  |
|   PR   |     399    |  420  |  25721 |
|   RJ   |     92     |  183  |  34068 |
|   RN   |     167    |  172  |  7674  |
|   RO   |     52     |  56   |  4198  |
|   RR   |     15     |  16   |  1268  |
|   RS   |     497    |  522  |  27201 |
|   SC   |     295    |  314  |  16242 |
|   SE   |     75     |  77   |  5498  |
|   SP   |     645    |  780  |  101073|
|   TO   |     139    |  140  |  3957  |
|   ZZ   |     181    |  181  |  1064  |

# TODO List - Tarefas a fazer

- Melhorar a documentação
