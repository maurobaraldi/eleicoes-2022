FROM python:3.10

ADD . /app/
WORKDIR /app/
RUN pip install celery redis

#celery -A tasks worker --pool=prefork --concurrency=4 --loglevel=info