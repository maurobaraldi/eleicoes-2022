# Download de urnas por UF
import time
import json
with open('./arquivos/lista_de_bus_rr.json') as _:
    bus = json.loads(_.read())

count = 0
for bu in bus[:2]:
    if count % 10 == 0:
        time.sleep(10)
        print("\nPausa de 10 segundos...\n")
    boletim_de_urna(*bu)
    count += 1


# Gera lista de totalizadoes de us x municipios x zonas x seçoes

import os
import json
total = {}
for i in os.listdir('./arquivos/zonas-secoes'):
    tzonas, tsecoes = 0, 0
    with open(f'./arquivos/zonas-secoes/{i}') as _:
    #with open(f'./arquivos/zonas-secoes/zonas-secoes-sp.json') as _:
        res = json.loads(_.read())
        municipios = [j for j in res["abr"][0]["mu"]]

        print(f'Estado: {res["abr"][0]["ds"]}')

        for m in municipios:
            secoes = 0
            for z in m["zon"]:
                secoes += len(z["sec"])
            print(f'  Municipio: {m["nm"]} {len(m["zon"])} zonas e {secoes} seções')
            tzonas += len(m["zon"])
            tsecoes += secoes
        
    total[res["abr"][0]["cd"]] = [len(municipios), tzonas, tsecoes]

for i in sorted(total):
    print(i, total[i])
