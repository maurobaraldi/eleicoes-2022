from glob import glob
from json import dumps, loads
from os import system
from os.path import exists
from urllib.request import Request, urlopen

url_base = "https://resultados.tse.jus.br/oficial/ele2022"
# A UF zz refere-se a dados de fora do Brasil (eleitores no exterior)
ufs = "ac al ap am ba ce df es go ma mt ms mg pr pb pa pe pi rj rn rs ro rr sc se sp to zz".split()

# Numeros dos registros na URL do site do TSE
turnos = {1: 544, 2: 545}

def presidente_simplificado():
    """Download dos dados consolidados do cargo de presidente por UF."""

    for id, turno in turnos.items():
        for uf in ufs:
            #print(f"https://resultados.tse.jus.br/oficial/ele2022/{turno}/dados-simplificados/{uf}/{uf}-c0001-e000{turno}-r.json")
            request = Request(f"{url_base}/{turno}/dados-simplificados/{uf}/{uf}-c0001-e000{turno}-r.json", headers={'User-agent': 'Mozilla/5.0'})
            with urlopen(request) as r:
                pasta = "primeiro-turno" if turno == 544 else "segundo-turno"
                with open(f"./arquivos/{pasta}/resultado-{uf}.json", "w") as fp:
                    fp.write(r.read().decode("utf8").strip().replace("\\", ""))
    print("Download de dados, simplificado, para presidente finalizado")

def zonas_e_secoes():
    """Download lista de zonas e seções elitorais por Cidade/UF."""

    for uf in ufs:
        request = Request(f"{url_base}/arquivo-urna/406/config/{uf}/{uf}-p000406-cs.json", headers={'User-agent': 'Mozilla/5.0'})
        with urlopen(request) as r:
            with open(f"./arquivos/zonas-secoes-{uf}.json", "w") as fp:
                fp.write(r.read().decode("utf8").strip())
    print("Download de lista de zonas e seções elitorais por Cidade/UF finalizado")

def boletim_de_urna(turno: int, uf: str, municipio: str, zona: str, secao: str):
    """Download do arquivo de boletim de urna."""

    print(f'Iniciando dowload do bu {uf}-{municipio} {turno}, {zona}, {secao}')
    turnos = {1: 406, 2: 407}
    url = url_base + f"/arquivo-urna/{turnos[turno]}/dados/{uf}/{municipio}/{zona}/{secao}"
    arquivo = f"/p000{turnos[turno]}-{uf}-m{municipio}-z{zona}-s{secao}-aux.json"

    # Arquivo auxiliar.
    headers = {
        'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 
        "Referrer": "https://resultados.tse.jus.br/oficial/app/index.html",
        'sec-ch-ua-platform': "macOS",
        'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
        'sec-ch-ua-mobile': '?0'
    }
    request = Request(f"{url}{arquivo}", headers=headers)
    with urlopen(request) as r:
        _hash = loads(r.read().decode("utf8").strip())["hashes"][0]["hash"]

    # Boletim de urna
    url = f"{url}/{_hash}/o00{turnos[turno]}-{municipio}{zona}{secao}.bu"
    pasta = "primeiro-turno" if 1 == 406 else "segundo-turno"
    download_cmd = f"curl -s '{url}' -H 'sec-ch-ua: \"Chromium\";v=\"106\", \"Google Chrome\";v=\"106\", \"Not;A=Brand\";v=\"99\"' -H 'Referer: https://resultados.tse.jus.br/oficial/app/index.html' -H 'sec-ch-ua-mobile: ?0' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36' -H 'sec-ch-ua-platform: \"macOS\"' --compressed --output ./arquivos/{pasta}/bus/o00{turnos[turno]}-{municipio}{zona}{secao}.bu"
    print(download_cmd)
    if exists(f'./arquivos/{pasta}/bus/o00{turnos[turno]}-{municipio}{zona}{secao}.bu') is False:
        system(download_cmd)
        print("Concluido")
        exit()
    else:
        print(f"{turnos[turno]}-{municipio}{zona}{secao} já baixado anteriormente.")

def gera_lista_boletins_de_urna():
    """
    Scraping de todos os arquivo de boletim de urna.
    
    OBS. Todos os arquivos estão disponíveis na pasta arquivos/bus/
    """

    lista_de_bus = []

    for turno in (1, 2):
        for uf in sorted(glob("./arquivos/zonas-secoes/_zonas-secoes-[a-z]*.json")):
            print(f"Download de BUs da UF: {uf[-7:-5].upper()} {turno}º turno")
            with open(uf) as fp:
                dados = loads(fp.read())["abr"][0]
                estado = dados["cd"].lower()
                municipios = dados["mu"]
                for m in municipios:
                    municipio = m["cd"]
                    zonas = m["zon"]
                    print(f"  Municipio: {m['nm']}")
                    for z in zonas:
                        #import pdb; pdb.set_trace()
                        zona = z["cd"]
                        secoes = z["sec"]
                        for s in secoes:
                            secao = s["ns"]
                            lista_de_bus.append([turno, estado, municipio, zona, secao])
    with open('./arquivos/lista_de_bus.json', 'w') as fp:
        fp.write(dumps(lista_de_bus))

'''
# Gera lista de totalizadoes de us x municipios x zonas x seçoes

import os
import json
total = {}
for i in os.listdir('./arquivos/zonas-secoes'):
    tzonas, tsecoes = 0, 0
    with open(f'./arquivos/zonas-secoes/{i}') as _:
    #with open(f'./arquivos/zonas-secoes/zonas-secoes-sp.json') as _:
        res = json.loads(_.read())
        municipios = [j for j in res["abr"][0]["mu"]]

        print(f'Estado: {res["abr"][0]["ds"]}')

        for m in municipios:
            secoes = 0
            for z in m["zon"]:
                secoes += len(z["sec"])
            print(f'  Municipio: {m["nm"]} {len(m["zon"])} zonas e {secoes} seções')
            tzonas += len(m["zon"])
            tsecoes += secoes
        
    total[res["abr"][0]["cd"]] = [len(municipios), tzonas, tsecoes]

for i in sorted(total):
    print(i, total[i])
'''